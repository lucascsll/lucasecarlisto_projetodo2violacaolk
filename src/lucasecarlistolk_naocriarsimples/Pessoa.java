/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucasecarlistolk_naocriarsimples;

/**
 *
 * @author Lucas
 */
public abstract class Pessoa{
  protected String xNome;
  protected Pessoa(){
    xNome = "Sem nome";
  }
  protected Pessoa(String nome){
    xNome = nome;
  }
  public String getNome(){
    return xNome;
  }
}
